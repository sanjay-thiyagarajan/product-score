## Product Score
| [Install](https://developer.atlassian.com/console/install/1e5e3a72-b5ad-4386-9442-86ce0309bb01/?signature=909776f5cc749fab4123ea3b4188c676ecf163c2380c89a208e0648f044b61b7&product=confluence) |  

A one-stop solution for analysing your apps from the most popular markets like Google Play, App Store and Atlassian Marketplace without the help of an analyst  
  
## About the Project
Analyzing product reviews when there are a lot is painful. Electricity as we know transformed a lot of industries in the past 100 years and now we are dependent on it. In the same way we wanted to use AI to make this particular task just a matter of few seconds
  
## Built with  
- [Forge](https://developer.atlassian.com/platform/forge/)
- [React JS](https://reactjs.org/)
- [Plotly](https://plotly.com/javascript/)
- [expert.ai](https://www.expert.ai/)
- [REST API](https://restfulapi.net/)
- [Flask](https://flask.palletsprojects.com/en/2.0.x/)
- [NLTK](https://www.nltk.org/) 

## Getting Started

### Prerequisites
- Make sure to have forge installed following these [instructions](https://developer.atlassian.com/platform/forge/set-up-forge/)


### Installing the app

Clone the project
```
git clone https://lazyCodes7@bitbucket.org/sanjay-thiyagarajan/product-score.git
```

Move to the static directory and run:
```
cd product-score
npm install
```

Build and deploy your app by running:
```
forge deploy
```

Install your app in an Atlassian site by running:

```
forge install
```

Develop your app by running `forge tunnel` to proxy invocations locally:

```
forge tunnel
```

### Possible errors while installing.
The manifest.yml file might need some tweakings in order to be used on to your own atlassian site. StackOverflow is the best help in this case!

### Installing on your Confluence site.
- Go to this [link](https://developer.atlassian.com/console/install/1e5e3a72-b5ad-4386-9442-86ce0309bb01/?signature=909776f5cc749fab4123ea3b4188c676ecf163c2380c89a208e0648f044b61b7&product=confluence) and install the app
- For more, [watch](https://youtu.be/pBgW9k0ND84) this video on how to install our app
## Usage
The app can be used to analyze apps from the Atlassian marketplace, App Store, Play Store.

### Atlassian marketplace
For analyzing an app on the atlassian marketplace, enter your app package something like down below.
```
com.atlassian.confluence.plugins.confluence-questions
```
### Google Play Store
For analyzing your google app enter a link that follows this type. Here id is the id of your app as on Play Store.
```
https://play.google.com/store/apps/details?id=com.viber.voip
```
### Apple's App Store
Something like this is what would work for App Store
```
https://apps.apple.com/in/app/apple-books/id364709193
```

## Services
Some of the services that we provide

- Behavioral traits, emotional traits, using NLP

- See the trends for your ratings

- Get your top keywords or IPTC Analysis from your reviews

- Segregate positive and negative reviews using Sentiment Analysis

- Interacting with the plots and saving them to device 

## Examples
#### Here is a demonstration of how the app sends personalized reports for a playstore app.

![](https://github.com/Techipeeyon/Images/raw/main/icons/deepin-screen-recorder-Select-ar%20(1).gif)

#### For a more detailed version which showcases our project properly, watch the demo on [YouTube](https://youtu.be/ousyEvftRmk).


